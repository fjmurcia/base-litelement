import {LitElement, html} from 'lit-element'

class FichaPersona extends LitElement {
    static get properties(){
        return {
            name: {type: String},
            yearsInCompany: {type: Number},
			personInfo: {type: String},            
            photo: {type: Object}            
        }
    }

    constructor(){
        super();
        this.name="Prueba nombre";
        this.yearsInCompany = 12;
		this.photo = {
			src: "./img/usuario.png",
			alt: "foto persona"			
        };        
        
        this.updatePersonInfo();
    }

    updated(changedProperties){
        changedProperties.forEach((oldValue, propName) => {
            console.log("ficha-persona.js ::: Propiedad: "+propName+" cambia valor, anterior era " + oldValue);
        });
        
        if(changedProperties.has("name")) {
            console.log("ficha-persona.js ::: Propiedad name cambia: " + changedProperties.get("name") + " nuevo: " + this.name);

        }
		if (changedProperties.has("yearsInCompany")) {
			console.log("ficha-persona.js ::: Propiedad yearsInCompany cambiada valor anterior era " + changedProperties.get("yearsInCompany") + " nuevo es " + this.yearsInCompany);
			this.updatePersonInfo();
		}        
    }

    

    render() {
        /*esto es el template*/
        return html`
            <div>
                <label>Nombre Completo</label>
                <input type="text" id="fname" value="${this.name}" @input="${this.updateName}"></input>
                <br/>
                <label>Años en empresa</label>        
                <input type="text" value="${this.yearsInCompany}" @input="${this.updateYearsInCompany}"></input>                      
                <br />			
				<input type="text" name="personInfo" value="${this.personInfo}" disabled></input>
                <br/>
				<img src="${this.photo.src}" height="200" width="200" alt="${this.photo.alt}">
            </div>
        `;
    }

    //Funcion manejadora
    updateName(e) {
        console.log("ficha-persona.js ::: updateName: " + e.target.value);
        this.name=e.target.value;
    }

    
	updateYearsInCompany(e) {
		console.log("ficha-persona.js :::updateyearsInCompany: "+ e.target.value);
        this.yearsInCompany = e.target.value;
        
    }
    
	updatePersonInfo() {
		console.log("ficha-persona.js ::: updatePersonInfo");
		console.log("ficha-persona.js ::: yearsInCompany vale " + this.yearsInCompany);

		if (this.yearsInCompany >= 7) {
			this.personInfo = "lead";
		} else if (this.yearsInCompany >= 5) {
			this.personInfo = "senior";
		} else if (this.yearsInCompany >= 3) {
			this.personInfo = "team";
		} else {
			this.personInfo = "junior";
		}
    }
        
}

/*aqui se declara el custom element*/
customElements.define('ficha-persona', FichaPersona)