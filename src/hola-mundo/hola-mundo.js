import {LitElement, html} from 'lit-element'

class HolaMundo extends LitElement {
    render() {
        /*esto es el template*/
        return html`
            <div>Hola Munda</div>
        `;
    }
}

/*aqui se declara el custom element*/
customElements.define('hola-mundo', HolaMundo)