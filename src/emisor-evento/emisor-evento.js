import {LitElement, html} from 'lit-element'

class EmisorEvento extends LitElement {
    render() {
        /*esto es el template*/
        return html`
            <div>Emisor Evento</div>	
            <button @click="${this.sendEvent}">No pulsar</button>		

        `;
    }

    sendEvent(e) {
        console.log("Pulsado el boton");

        this.dispatchEvent(
            new CustomEvent(
                "test-event",
                {
                    "detail": {
                        course: "TechU",
                        year: 2020
                    }
                }
            )
        );

    }
}

    
/*aqui se declara el custom element*/
customElements.define('emisor-evento', EmisorEvento)