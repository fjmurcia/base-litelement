import {LitElement, html} from 'lit-element'
import '../persona-header/persona-header.js';
import '../persona-main/persona-main.js'; 
import '../persona-footer/persona-footer.js'; 
import '../persona-sidebar/persona-sidebar.js'; 
import '../persona-stats/persona-stats.js'; 


class PersonaApp extends LitElement {

    static get properties() {
        return {			
            people: {type: Array}
        };
    }	


    render() {
        /*esto es el template*/
        return html`
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
            <persona-header></persona-header>
            <div class="row">
                <persona-sidebar @updated-max-years-filter="${this.updateFilterMaxYears}" class="col-2" @new-person="${this.newPerson}"></persona-sidebar>                
                <persona-main @updated-people="${this.updatedPeople}" class="col-10"></persona-main>

            </div>
            <persona-footer></persona-footer>
            <persona-stats @updated-people-stats="${this.updatedPeopleStats}"></persona-stats>

        `;
    }

    updated(changedProperties){
        console.log("persona-app.js ::: updated");

        if(changedProperties.has("people"))
        {
            console.log("persona-app.js ::: people updated");
            this.shadowRoot.querySelector("persona-stats").people = this.people;
        }
    }

    updateFilterMaxYears(e)
    {
        console.log("persona-app.js ::: updateFilterMaxYears");
        this.shadowRoot.querySelector("persona-main").maxYearsInCompany = e.detail.maxYearsInCompany;
    }

    newPerson(e){
        console.log("persona-app.js ::: newPerson en persona-app");
        this.shadowRoot.querySelector("persona-main").showPersonForm = true;
    }

    updatedPeopleStats(e){
        console.log("persona-app.js ::: updatedPeopleStats");        
        this.shadowRoot.querySelector("persona-sidebar").peopleStats = e.detail.peopleStats;
        this.shadowRoot.querySelector("persona-main").maxYearsInCompany = e.detail.peopleStats.maxYearsInCompany;
    }

    updatedPeople(e){
        console.log("persona-app.js ::: updatedPeople");        
        this.people = e.detail.people;
    }    

}

/*aqui se declara el custom element*/
customElements.define('persona-app', PersonaApp)