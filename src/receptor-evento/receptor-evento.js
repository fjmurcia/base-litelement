import {LitElement, html} from 'lit-element'

class ReceptorEvento extends LitElement {

    static get properties() {
        return {
            course: {type: String},
            year: {type: String}
        }
    }

    render() {
        /*esto es el template*/
        return html`
            <h3>Rx Evento</h3>
            <h5>Este curso es de ${this.course}</h5>	
            <h5>... estamos en el año ${this.year}</h5>	

        `;
    }



}

    
/*aqui se declara el custom element*/
customElements.define('receptor-evento', ReceptorEvento)