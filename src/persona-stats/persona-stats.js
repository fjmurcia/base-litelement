import {LitElement, html} from 'lit-element'


class PersonaStats extends LitElement {

    static get properties() {
        return {			
            people: {type: Array}
        };
    }	

    constructor()
    {
        super();
        //inicializar array
        this.people=[];
    }


    //gestionamos cuando cambien las propiedades 
    updated(changedProperties)
    {
        console.log("persona-stats.js ::: updated()");
        if(changedProperties.has("people"))
        {
            console.log("persona-stats.js ::: people updated");
            let peopleStats = this.gatherPeopleArrayInfo(this.people);

            this.dispatchEvent(new CustomEvent("updated-people-stats", {
                detail : {
                    peopleStats: peopleStats
                }
            }
            ));
        }

    }

    gatherPeopleArrayInfo(people) {
        console.log("persona-stats.js ::: gatherPeopleArrayInfo");
        let peopleStats = {};
        peopleStats.numberOfPeople = people.length;

        let maxYearsInCompany = 0;

        people.forEach(person => {
            maxYearsInCompany = person.yearsInCompany > maxYearsInCompany ? person.yearsInCompany : maxYearsInCompany;
        });

        console.log("persona-stats.js ::: maxYearsInCompany : " + maxYearsInCompany);

        peopleStats.maxYearsInCompany = maxYearsInCompany;
        
        return peopleStats;
    }

}

    
/*aqui se declara el custom element*/
customElements.define('persona-stats', PersonaStats)