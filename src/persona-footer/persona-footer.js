import {LitElement, html} from 'lit-element'

class PersonaFooter extends LitElement {
    render() {
        /*esto es el template*/
        return html`
            <h5>@PersonaApp 2020</h5>
        `;
    }

}


/*aqui se declara el custom element*/
customElements.define('persona-footer', PersonaFooter)