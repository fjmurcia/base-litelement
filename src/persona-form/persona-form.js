import {LitElement, html} from 'lit-element'

class PersonaForm extends LitElement {
    static get properties() {
        return {
            person: {type: Object},
            editingPerson: {type: Boolean}
        }
    }


    constructor() {
        super();
        this.resetFormData();
    }


    render() {
        /*esto es el template*/
        return html`
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <div>
            <form>
                <div class="form-group">
                    <label>Nombre</label>
                    <input type="text" id="personFormName" class="form-control" placeholder="Nombre" 
                    @input="${this.updateName}"        
                    .value="${this.person.name}" 
                    ?disabled="${this.editingPerson}"/>
                </div>
                <div class="form-group">
                    <label>Perfil</label>
                    <textarea rows="5" class="form-control" placeholder="Pefil" 
                    @input="${this.updateProfile}"
                    .value="${this.person.profile}">
                    </textarea>
                </div>
                <div class="form-group">
                    <label>Antigüedad</label>
                    <input type="text" class="form-control" placeholder="Antigüedad" 
                    @input="${this.updateYearsInCompany}" 
                    .value="${this.person.yearsInCompany}"/>
                </div>

                <button class="btn btn-primary" @click="${this.goBack}"><strong>Back</strong></button>
                <button class="btn btn-primary" @click="${this.storePerson}"><strong>Guardar</strong></button>                

            </form>
        </div>

        `;
    }

    resetFormData()
    {
        console.log("persona-form.js ::: Reset Form Data");
        this.person = {};
        this.person.name = "";
        this.person.profile = "";
        this.person.yearsInCompany = "";        
        this.editingPerson=false;
    }

    updateName(e)
    {
        this.person.name = e.target.value;

    }

    updateProfile(e)
    {
        this.person.profile = e.target.value;
    }

    updateYearsInCompany(e)
    {
        this.person.yearsInCompany = e.target.value;
    }

    goBack(e)
    {
        console.log("persona-form.js ::: goBack");
        e.preventDefault();
        this.resetFormData();
        this.dispatchEvent(new CustomEvent("persona-form-close", {}));


    }

    storePerson(e)
    {
        console.log("persona-form.js ::: storePerson");
        e.preventDefault();

        this.person.photo = {
            src: "./img/usuario.png",
            alt: "Persona"
        }

        console.log("persona-form ::: " +this.person.name +":" +this.person.profile +":" +this.person.yearsInCompany);

        
        this.dispatchEvent(new CustomEvent("persona-form-store", {
            detail: {
                person: {
                    name: this.person.name,
                    profile: this.person.profile,
                    yearsInCompany: this.person.yearsInCompany,
                    photo: this.person.photo
                },
                editingPerson : this.editingPerson
            }
        }));

        this.resetFormData();
    }    
}


/*aqui se declara el custom element*/
customElements.define('persona-form', PersonaForm)