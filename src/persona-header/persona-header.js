import {LitElement, html} from 'lit-element'

class PersonaHeader extends LitElement {
    render() {
        /*esto es el template*/
        return html`
            <h1>App Molona</h1>
        `;
    }

}


/*aqui se declara el custom element*/
customElements.define('persona-header', PersonaHeader)