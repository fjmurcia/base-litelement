import {LitElement, html} from 'lit-element'

class TestApi extends LitElement {

    static get properties(){
        return {
            movies: {type: Array}
        };
    }

    constructor(){
        super();
        this.movies = [];
        this.getMovieData();
    }
    render() {
        /*esto es el template*/
        return html`
        ${this.movies.map(
            movie => html`<div>peli ${movie.title} dir ${movie.director} </div>` 
        )}

        `;
    }

    getMovieData(){
        console.log("datos pelis");
        //let a nivel de bloque, var a nivel de funcion y sin nada global
        let xhr = new XMLHttpRequest();
        xhr.onload = () => {
            if(xhr.status===200)
            {
                console.log("HTTP200 - Peticion completada");                
                //console.log(xhr.responseText);
                let APIResponse = JSON.parse(xhr.responseText);
                this.movies=APIResponse.results;
            }
        };
        //open prepara la peti, configuracion, etc
        xhr.open("GET", "https://swapi.dev/api/films/");
        xhr.send(/*aqui se envia el body*/);
        console.log("Fin de movies")
    }



}

    
/*aqui se declara el custom element*/
customElements.define('test-api', TestApi)